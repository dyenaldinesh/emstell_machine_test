package com.emstelltest

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ImageView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.emstelltest.Adapter.ProductAdapter
import com.emstelltest.Model.Products.Product
import com.emstelltest.Model.Products.ProductRequest
import com.emstelltest.Model.Products.ProductResponse
import com.emstelltest.Util.CircleTransform
import com.quattroteq.bikewallpapernew.Network.RetroClient
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class MainActivity : AppCompatActivity() {
    private lateinit var linearLayoutManager: LinearLayoutManager
    val productlist : MutableList<Product> = ArrayList()
    private lateinit var productadapter: ProductAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initRecyclerView()

        val request = RetroClient.apiService.getProducts(ProductRequest("69","Android","69",
                "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJjdXN0b21lcl9pZCI6IjY5IiwiaWF0IjoxNjA2ODgzNjE3fQ.QK13epjPKnc-twJCwObDVNenfPsmw2HR1f9lJUG3CEo",
                "1","225",
                "1716","1",
                "6","2"))
      request.enqueue(object: Callback<ProductResponse> {
          override fun onFailure(call: Call<ProductResponse>, t: Throwable) {
              progressBar.visibility=View.GONE
          }

          override fun onResponse(call: Call<ProductResponse>, response: Response<ProductResponse>) {
              progressBar.visibility=View.GONE
              if(response.isSuccessful) {
                  productlist.addAll(response.body()!!.products!!)
                  productadapter.notifyDataSetChanged()
                  tv_badge.text= response.body()!!.sellers!!.get(0).productCount
                  tv_title.text = response.body()!!.sellers!!.get(0).sellerName!!.toUpperCase()



                  Picasso.get().load(
                      "https://d2tcyife2dgcgh.cloudfront.net/image/" + response.body()!!.sellers!!.get(
                          0).logo)
                      .transform(CircleTransform())
                      .into(img_logo) }
          }

      })


    }

    private fun initRecyclerView() {
        productadapter = ProductAdapter(productlist,application)
        linearLayoutManager = LinearLayoutManager(this)
        recyclerview.layoutManager = linearLayoutManager
        recyclerview.adapter=productadapter
    }
}