package com.emstelltest.Model.ProductDetails

data class ProductDetailRequest(var user_id: String,var device_type : String,
                                var customer_id:String,var token:String,var language_id:String,
                                var country_id:String,var area_id:String,var product_id:String)