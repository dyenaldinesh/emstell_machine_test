package com.emstelltest.Model.Products

data class ProductRequest(var user_id: String,var device_type : String,
                          var customer_id:String,var token:String,var language_id:String,
                          var country_id:String,var area_id:String,var main_category_id:String,
                          var category_id_l2:String,var category_id_l1:String)