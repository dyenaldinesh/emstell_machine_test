package com.emstelltest.Model.Products


import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class ProductResponse {
    @SerializedName("status")
    @Expose
    var status: String? = null

    @SerializedName("sellers")
    @Expose
    var sellers: List<Seller>? = null

    @SerializedName("products")
    @Expose
    var products: List<Product>? = null

}