package com.emstelltest.Model.Products

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Seller {
    @SerializedName("seller_id")
    @Expose
    var sellerId: String? = null

    @SerializedName("seller_name")
    @Expose
    var sellerName: String? = null

    @SerializedName("image")
    @Expose
    var image: String? = null

    @SerializedName("logo")
    @Expose
    var logo: String? = null

    @SerializedName("product_count")
    @Expose
    var productCount: String? = null

}