package com.emstelltest.Model.Products

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Product {
    @SerializedName("product_id")
    @Expose
    var productId: String? = null

    @SerializedName("name")
    @Expose
    var name: String? = null

    @SerializedName("seller_name")
    @Expose
    var sellerName: String? = null

    @SerializedName("image")
    @Expose
    var image: String? = null

    @SerializedName("price")
    @Expose
    var price: String? = null

    @SerializedName("quantity")
    @Expose
    var quantity: String? = null

    @SerializedName("discount_status")
    @Expose
    var discountStatus: String? = null

    @SerializedName("discount_percentage")
    @Expose
    var discountPercentage: String? = null

    @SerializedName("rating")
    @Expose
    var rating: Any? = null

    @SerializedName("fav")
    @Expose
    var fav: String? = null

    @SerializedName("color_options")
    @Expose
    var colorOptions: List<ColorOption>? = null

    @SerializedName("old_price")
    @Expose
    var oldPrice: String? = null

}