
package com.emstelltest.Model.Products;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ColorOption {

    @SerializedName("color_code")
    @Expose
    private String colorCode;

    public String getColorCode() {
        return colorCode;
    }

    public void setColorCode(String colorCode) {
        this.colorCode = colorCode;
    }

}
