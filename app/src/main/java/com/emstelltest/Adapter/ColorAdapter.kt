package com.emstelltest.Adapter

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.annotation.NonNull
import androidx.core.graphics.BlendModeColorFilterCompat
import androidx.core.graphics.BlendModeCompat
import androidx.recyclerview.widget.RecyclerView
import com.emstelltest.Model.ProductDetails.ProductOption
import com.emstelltest.Model.ProductDetails.ProductOptionValue
import com.emstelltest.R


internal class ColorAdapter(
    var context: Context,

    var  productOptionValue: MutableList<ProductOptionValue>?
) :
        RecyclerView.Adapter<ColorAdapter.MyViewHolder>() {
    var selected_item=-1
    internal inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var colorview: LinearLayout = view.findViewById(R.id.linear_color)
        var border: LinearLayout = view.findViewById(R.id.border_layout)


    }
    @NonNull
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
       val itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.colorlist_payload, parent, false)
        return MyViewHolder(itemView)
    }
    override fun onBindViewHolder(holder: MyViewHolder, position: Int)
    {

        var  colorDrawable = context.getDrawable(R.drawable.circle);
        colorDrawable?.setColorFilter(BlendModeColorFilterCompat.createBlendModeColorFilterCompat(Color.parseColor(
            productOptionValue!!.get(position).colorCode), BlendModeCompat.SRC_ATOP));
        holder.colorview.background=colorDrawable

        if (selected_item == position) {
            holder.border.background=context.getDrawable(R.drawable.border);

        } else {
            holder.border.setBackgroundResource(0)
        }


        holder.border.setOnClickListener {
            if(selected_item==position){
                selected_item=-1;
                notifyDataSetChanged();

            }
            selected_item = position;
            notifyDataSetChanged();


        }

    }
    override fun getItemCount(): Int {
        return productOptionValue!!.size
    }



}