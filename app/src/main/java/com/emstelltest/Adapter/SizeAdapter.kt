package com.emstelltest.Adapter

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.annotation.NonNull
import androidx.core.graphics.BlendModeColorFilterCompat
import androidx.core.graphics.BlendModeCompat
import androidx.recyclerview.widget.RecyclerView
import com.emstelltest.Model.ProductDetails.ProductOption
import com.emstelltest.Model.ProductDetails.ProductOptionValue
import com.emstelltest.R


internal class SizeAdapter(
    var context: Context,
    var  productsize: MutableList<ProductOptionValue>?
) :
        RecyclerView.Adapter<SizeAdapter.MyViewHolder>() {
    var selected_item=-1
    internal inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var textSize: TextView = view.findViewById(R.id.tv_size)
        var border: LinearLayout = view.findViewById(R.id.border_layout)
    }
    @NonNull
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
       val itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.sizelist_payload, parent, false)
        return MyViewHolder(itemView)
    }
    override fun onBindViewHolder(holder: MyViewHolder, position: Int)
    {

        holder.textSize.text= productsize!!.get(position).name
        if (selected_item == position) {
            holder.border.background=context.getDrawable(R.drawable.border);

        } else {
            holder.border.setBackgroundResource(0)
        }


        holder.border.setOnClickListener {
            if(selected_item==position){
                selected_item=-1;
                notifyDataSetChanged();

            }
            selected_item = position;
            notifyDataSetChanged();


        }

    }
    override fun getItemCount(): Int {
        return productsize!!.size
    }



}