package com.emstelltest.Adapter

import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.NonNull
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat.startActivity
import androidx.recyclerview.widget.RecyclerView
import com.emstelltest.Model.Products.Product
import com.emstelltest.ProductDetailsActivity
import com.emstelltest.R
import com.squareup.picasso.Picasso

internal class ProductAdapter(private var products: List<Product>?,var context: Context) :
        RecyclerView.Adapter<ProductAdapter.MyViewHolder>() {
    internal inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var name: TextView = view.findViewById(R.id.tv_name)
        var seller: TextView = view.findViewById(R.id.tv_sellername)
        var amount: TextView = view.findViewById(R.id.tv_amount)
        var rating: TextView = view.findViewById(R.id.tv_rating)
        var image: ImageView = view.findViewById(R.id.img_product)
        var layout: ConstraintLayout = view.findViewById(R.id.main_layout)
    }
    @NonNull
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
       val itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.product_payload, parent, false)
        return MyViewHolder(itemView)
    }
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        Log.e("images",products!!.get(position).image)
        holder.name.text = products!!.get(position).name
        holder.amount.text = "$ "+products!!.get(position).price
        holder.seller.text = products!!.get(position).sellerName
        if(products!!.get(position).rating!=null)
        holder.rating.text = products!!.get(position).rating.toString()
        /** item image URL has some issue thats why it is hardcoded**/

        Picasso.get().load("https://i.pinimg.com/originals/f3/0e/8b/f30e8b236fff35e4e71e4428d808324c.png").into(holder.image)
        holder.layout.setOnClickListener{ v: View? ->
            if (v != null) {
                val intent =Intent(v.context, ProductDetailsActivity::class.java)
                intent.putExtra("productId", products!!.get(position).productId)
                v.context.startActivity(intent)
            }
        }
    }
    override fun getItemCount(): Int {
        return products?.size!!
    }
}