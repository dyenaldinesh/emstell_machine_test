
package com.emstelltest.Model


import com.emstelltest.Model.ProductDetails.ProductDetailRequest
import com.emstelltest.Model.ProductDetails.ProductDetailsResponse
import com.emstelltest.Model.Products.ProductRequest
import com.emstelltest.Model.Products.ProductResponse
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

interface ApiService {

    @POST("get_top_sellers_products")
    fun getProducts(@Body productRequest: ProductRequest):Call<ProductResponse>

    @POST("get_product_details")
    fun getProductsDetails(@Body productDetailRequest: ProductDetailRequest):Call<ProductDetailsResponse>

}
