package com.quattroteq.bikewallpapernew.Network

import com.emstelltest.Model.ApiService
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RetroClient {

    /********
     * URLS
     */
    private val ROOT_URL = "https://beta.gastro-admin.xyz/mobileapi/retail/"

    /**
     * Get Retrofit Instance
     */
    private val retrofitInstance: Retrofit
        get() = Retrofit.Builder()
            .baseUrl(ROOT_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

    /**
     * Get API Service
     *
     * @return API Service
     */
    val apiService: ApiService
        get() = retrofitInstance.create(ApiService::class.java)
}