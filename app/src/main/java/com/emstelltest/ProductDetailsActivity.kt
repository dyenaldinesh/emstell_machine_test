package com.emstelltest

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.emstelltest.Adapter.ColorAdapter
import com.emstelltest.Adapter.SizeAdapter
import com.emstelltest.Model.ProductDetails.*
import com.quattroteq.bikewallpapernew.Network.RetroClient
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_product_details.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ProductDetailsActivity : AppCompatActivity() {
    var count=1
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product_details)
        initView()
        InitApi()
    }

    private fun initView() {
        btn_back.setOnClickListener {
            finish()
        }
        btn_add.setOnClickListener {

            tv_count.text=(++count).toString()
        }
        btn_remove.setOnClickListener {
            if(count>=1)
            tv_count.text=(--count).toString()
        }
    }


    private fun InitApi() {
        val request = RetroClient.apiService.getProductsDetails(
            ProductDetailRequest("69","Android","69",
            "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJjdXN0b21lcl9pZCI6IjY5IiwiaWF0IjoxNjA2ODgzNjE3fQ.QK13epjPKnc-twJCwObDVNenfPsmw2HR1f9lJUG3CEo",
            "1","225",
            "1716",intent.getStringExtra("productId")))
        request.enqueue(object :Callback<ProductDetailsResponse>{
            override fun onFailure(call: Call<ProductDetailsResponse>, t: Throwable) {
                Log.e("Apiresponse",t.message)
                progress_detail.visibility=View.GONE
            }

            override fun onResponse(call: Call<ProductDetailsResponse>, response: Response<ProductDetailsResponse>) {
                progress_detail.visibility=View.GONE
                if (response.isSuccessful)
                {
                    var productDetails:ProductDetails= response.body()!!.productDetails
                    var productColor: MutableList<ProductOptionValue>? = response.body()!!.productOptions[0].productOptionValue
                    var productSize: MutableList<ProductOptionValue>? = response.body()!!.productOptions[1].productOptionValue
                    tv_description.text= productDetails.description
                    tv_price.text="$ "+productDetails.price
                    if(productDetails.rating!=null)
                    tv_rating.text=productDetails.rating.toString()
                    tv_name.text=productDetails.name

                    recyclerview.layoutManager=LinearLayoutManager(applicationContext,LinearLayoutManager.HORIZONTAL, false)
                    recyclerview.adapter=ColorAdapter(applicationContext,productColor)

                    recycle_size.layoutManager=LinearLayoutManager(applicationContext,LinearLayoutManager.HORIZONTAL, false)
                    recycle_size.adapter=SizeAdapter(applicationContext,productSize)


                    Picasso.get().load("https://i.pinimg.com/originals/f3/0e/8b/f30e8b236fff35e4e71e4428d808324c.png").into(img_main)
                }

            }

        })
    }
}